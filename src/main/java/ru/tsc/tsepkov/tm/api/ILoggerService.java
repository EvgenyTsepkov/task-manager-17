package ru.tsc.tsepkov.tm.api;

public interface ILoggerService {

    void info(String message);

    void  command(String message);

    void debug(String message);

    void error(Exception e);

}
