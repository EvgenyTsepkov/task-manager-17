package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();

    int getSize();

}
