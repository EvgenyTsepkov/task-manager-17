package ru.tsc.tsepkov.tm.command.system;

import ru.tsc.tsepkov.tm.command.AbstractCommand;

public class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String DESCRIPTION = "Close application.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
