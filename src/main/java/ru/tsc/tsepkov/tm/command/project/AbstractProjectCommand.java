package ru.tsc.tsepkov.tm.command.project;

import ru.tsc.tsepkov.tm.api.IProjectService;
import ru.tsc.tsepkov.tm.api.IProjectTaskService;
import ru.tsc.tsepkov.tm.command.AbstractCommand;
import ru.tsc.tsepkov.tm.enumerated.Status;
import ru.tsc.tsepkov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
