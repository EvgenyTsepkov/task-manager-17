package ru.tsc.tsepkov.tm.command.project;

import ru.tsc.tsepkov.tm.model.Project;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-show-by-id";

    public static final String DESCRIPTION = "Display project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        showProject(project);
    }

}
